package main

import (
	"fmt"
	"os"
)

func main() {
	name := os.Getenv("INPUT_NAME")
	message := os.Getenv("INPUT_MESSAGE")
	greeting := message + " " + name
	data := "greeting=" + greeting
	os.WriteFile(os.Getenv("OUTPUT_FILE"), []byte(data), 0666)
	data = "USER=" + name
	os.WriteFile(os.Getenv("ENV_FILE"), []byte(data), 0666)
	fmt.Println(greeting)
}
